package xyz.sky731.programming.lab3;

public interface Workable {
    public StatusOfDepartment work();
}
